package com.example.romul.appbomba

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast

class MenuDias : AppCompatActivity() {
    lateinit var mRecyclerView : RecyclerView
    var mAdapter : Recycler = Recycler()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val actionBar = supportActionBar
        if(actionBar != null){
            actionBar.setTitle("Proximos eventos")
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setHomeButtonEnabled(true)
            actionBar.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        setUpRecyclerView()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the main_menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }
    override  fun onOptionsItemSelected( item: MenuItem): Boolean{
        return when (item.getItemId()){
                    android.R.id.home ->
                    {
                        System.exit(0)
                        true
                    }
            R.id.ActivarNotify -> {
                try {
                    val intent = Intent(this, ServiceAppBomba::class.java)
                    startService(intent)
                    Toast.makeText(this,"Notificacion Activada", Toast.LENGTH_LONG).show()
                }catch(ex:Exception){
                    Toast.makeText(this,"Error:${ex.message}", Toast.LENGTH_LONG).show()
                }

                true
            }
            R.id.DesactivarNotify ->{
                try {
                    val intent = Intent(this,ServiceAppBomba::class.java)
                    stopService(intent)
                    Toast.makeText(this,"Notificacion Desactivada",Toast.LENGTH_LONG).show()
                }catch (ex:Exception){
                    Toast.makeText(this,"Error:${ex.message}",Toast.LENGTH_LONG).show()
                }
                true
            }
                    else -> super.onOptionsItemSelected(item)
        }
    }
    fun setUpRecyclerView(){
        mRecyclerView = findViewById(R.id.Vlista)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mAdapter.Recycler(getPersonas(), this)
        mRecyclerView.adapter = mAdapter
    }

    fun getPersonas(): MutableList<menuConfig> {
        var opciones:MutableList<menuConfig> = ArrayList()
        opciones.add(menuConfig("Lunes", "15:20", "18:50"))
        opciones.add(menuConfig("Martes", "18:00", "20:41"))
        opciones.add(menuConfig("Miercoles", "15:20", "18:50"))
        opciones.add(menuConfig("Jueves", "18:00", "20:41"))
        opciones.add(menuConfig("Viernes", "15:20", "18:50"))
        opciones.add(menuConfig("Sabado", "18:00", "20:41"))
        opciones.add(menuConfig("Domingo", "15:20", "18:50"))
        return opciones
    }

    override fun onBackPressed() {
        finish()
    }
}
