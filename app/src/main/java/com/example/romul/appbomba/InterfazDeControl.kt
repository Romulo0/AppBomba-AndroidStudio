package com.example.romul.appbomba

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.animation.ObjectAnimator
import android.graphics.drawable.Animatable
import android.graphics.drawable.Drawable
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast


class InterfazDeControl : AppCompatActivity() {
    val sms:MetodosParaSMS = MetodosParaSMS(this)
    var stop = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_interfaz_de_control)
        val mEncender = findViewById<Button>(R.id.btnEncender)
        val mImgCheck = findViewById<ImageView>(R.id.imgEncender)
        mEncender.setOnClickListener {
            try {
                if(!stop){
                    mImgCheck.setImageDrawable(getDrawable(R.drawable.animtarget_play))
                    (mImgCheck.drawable as Animatable).start()
                    stop = true
                    sms.enviarSMS_EncenderOApagar(true)
                }else{
                    mImgCheck.setImageDrawable(getDrawable(R.drawable.animtarget_stop))
                    (mImgCheck.drawable as Animatable).start()
                    stop = false
                    sms.enviarSMS_EncenderOApagar(false)
                }
            }catch(ex:Exception){
                Toast.makeText(this,ex.message,Toast.LENGTH_LONG).show()
            }
        }
    }
}
