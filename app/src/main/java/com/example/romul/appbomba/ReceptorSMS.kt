package com.example.romul.appbomba
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v4.app.NotificationCompat
import android.telephony.SmsMessage
import android.widget.Toast

class ReceptorSMS:BroadcastReceiver(){
    override fun onReceive(context: Context?, intent: Intent?) {
        try{
            val b = intent?.extras
            val pdus = b?.get("pdus") as Array<Any?>
            var idMensaje =""
            var textoMensaje = ""
            val mensajes = arrayOfNulls<SmsMessage>(pdus.size)
            for (i in mensajes.indices) {
                mensajes[i] = SmsMessage.createFromPdu(pdus[i] as ByteArray)
                if( mensajes[i]?.getOriginatingAddress() != "04263535067")
                    continue
                idMensaje += "SMS From: " + mensajes[i]?.getOriginatingAddress() + "\n"
                textoMensaje += mensajes[i]?.getMessageBody() + "\n"
            }
            if(textoMensaje.isEmpty())
                return
            GenerateNotification(idMensaje,textoMensaje,context)
        }catch (ex:Exception){
            Toast.makeText(context,ex.message, Toast.LENGTH_LONG).show()
        }
    }
    private fun GenerateNotification(id: String,texto:String, context:Context?) {
        try {
            val mBuilder = NotificationCompat.Builder(context)
                    .setAutoCancel(true)
                    .setContentTitle("Control Remoto de Bomba")
                    .setSubText(id)
                    .setContentText(texto)
                    .setSmallIcon(R.drawable.bombalogo)
            mBuilder.build()
            val manager = context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.notify(1, mBuilder.build())
        }catch (ex:Exception){
            TODO(ex.message.toString())
        }
    }
}