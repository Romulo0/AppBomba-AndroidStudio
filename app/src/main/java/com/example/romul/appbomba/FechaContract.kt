package com.example.romul.appbomba
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import android.content.ContentValues
import android.widget.Toast


/**
 * Created by romul on 04/03/2018.
 */
class FechaContract {
    class FechaEntrada : BaseColumns{
        companion object {
            val LUNES = "Lunes"
            val MARTES = "Martes"
            val MIERCOLES = "Miercoles"
            val JUEVES = "Jueves"
            val VIERNES = "Viernes"
            val SABADO = "Sabado"
            val DOMINGO = "Domingo"
            val USERDATA = "UserData"
            val USERPARAMETERS = "UserParameters"
            val ID = "id"
            val INICIO = "inicio"
            val FINAL = "final"
            val EVENTO = "Evento"
            val USER = "User"
            val PASSWORD = "Password"
            val NOTIFICATION = "Notification"
            val NUMEROSMS = "NumeroSms"
        }
    }
}
class FechaDbHelper(val context:Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION){
    companion object{
        val DATABASE_VERSION = 1
        val DATABASE_NAME = "AppBomba.db"
    }
    override fun onCreate(sqLiteDatabase: SQLiteDatabase){
        sqLiteDatabase.execSQL("CREATE TABLE " + FechaContract.FechaEntrada.LUNES + " ("
                + FechaContract.FechaEntrada.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + FechaContract.FechaEntrada.INICIO + " TEXT NOT NULL,"
                + FechaContract.FechaEntrada.FINAL + " TEXT NOT NULL,"
                + FechaContract.FechaEntrada.EVENTO + " TEXT NULL,"
                + "UNIQUE (" + FechaContract.FechaEntrada.ID + "))")
        sqLiteDatabase.execSQL("CREATE TABLE " + FechaContract.FechaEntrada.MARTES + " ("
                + FechaContract.FechaEntrada.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + FechaContract.FechaEntrada.INICIO + " TEXT NOT NULL,"
                + FechaContract.FechaEntrada.FINAL + " TEXT NOT NULL,"
                + FechaContract.FechaEntrada.EVENTO + " TEXT NULL,"
                + "UNIQUE (" + FechaContract.FechaEntrada.ID + "))")
        sqLiteDatabase.execSQL("CREATE TABLE " + FechaContract.FechaEntrada.MIERCOLES + " ("
                + FechaContract.FechaEntrada.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + FechaContract.FechaEntrada.INICIO + " TEXT NOT NULL,"
                + FechaContract.FechaEntrada.FINAL + " TEXT NOT NULL,"
                + FechaContract.FechaEntrada.EVENTO + " TEXT NULL,"
                + "UNIQUE (" + FechaContract.FechaEntrada.ID + "))")
        sqLiteDatabase.execSQL("CREATE TABLE " + FechaContract.FechaEntrada.JUEVES + " ("
                + FechaContract.FechaEntrada.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + FechaContract.FechaEntrada.INICIO + " TEXT NOT NULL,"
                + FechaContract.FechaEntrada.FINAL + " TEXT NOT NULL,"
                + FechaContract.FechaEntrada.EVENTO + " TEXT NULL,"
                + "UNIQUE (" + FechaContract.FechaEntrada.ID + "))")
        sqLiteDatabase.execSQL("CREATE TABLE " + FechaContract.FechaEntrada.VIERNES + " ("
                + FechaContract.FechaEntrada.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + FechaContract.FechaEntrada.INICIO + " TEXT NOT NULL,"
                + FechaContract.FechaEntrada.FINAL + " TEXT NOT NULL,"
                + FechaContract.FechaEntrada.EVENTO + " TEXT NULL,"
                + "UNIQUE (" + FechaContract.FechaEntrada.ID + "))")
        sqLiteDatabase.execSQL("CREATE TABLE " + FechaContract.FechaEntrada.SABADO + " ("
                + FechaContract.FechaEntrada.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + FechaContract.FechaEntrada.INICIO + " TEXT NOT NULL,"
                + FechaContract.FechaEntrada.FINAL + " TEXT NOT NULL,"
                + FechaContract.FechaEntrada.EVENTO + " TEXT NULL,"
                + "UNIQUE (" + FechaContract.FechaEntrada.ID + "))")
        sqLiteDatabase.execSQL("CREATE TABLE " + FechaContract.FechaEntrada.DOMINGO + " ("
                + FechaContract.FechaEntrada.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + FechaContract.FechaEntrada.INICIO + " TEXT NOT NULL,"
                + FechaContract.FechaEntrada.FINAL + " TEXT NOT NULL,"
                + FechaContract.FechaEntrada.EVENTO + " TEXT NULL,"
                + "UNIQUE (" + FechaContract.FechaEntrada.ID + "))")
        sqLiteDatabase.execSQL("CREATE TABLE " + FechaContract.FechaEntrada.USERDATA + " ("
                + FechaContract.FechaEntrada.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + FechaContract.FechaEntrada.USER + " TEXT NOT NULL,"
                + FechaContract.FechaEntrada.PASSWORD + " TEXT NOT NULL,"
                + "UNIQUE (" + FechaContract.FechaEntrada.ID + "))")
        sqLiteDatabase.execSQL("CREATE TABLE " + FechaContract.FechaEntrada.USERPARAMETERS + " ("
                + FechaContract.FechaEntrada.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + FechaContract.FechaEntrada.NOTIFICATION + " INTEGER NOT NULL,"
                + FechaContract.FechaEntrada.NUMEROSMS + " TEXT NOT NULL,"
                + "UNIQUE (" + FechaContract.FechaEntrada.ID + "))")
        sqLiteDatabase.execSQL("INSERT INTO " + FechaContract.FechaEntrada.USERPARAMETERS + " ("
                + FechaContract.FechaEntrada.NOTIFICATION + " ,"
                + FechaContract.FechaEntrada.NUMEROSMS + " )"
                + "VALUES (0,'0424-576-5881')")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
    }
//region CRUD
    fun getHoras(dia: String):MutableList<DataMenuHoras> {
        val db = this.readableDatabase
        val query = "SELECT  ${FechaContract.FechaEntrada.INICIO},${FechaContract.FechaEntrada.FINAL} " +
                "FROM $dia"
        val cursor = db.rawQuery(query, null)
        val receivedHoras : MutableList<DataMenuHoras> = ArrayList()
       if (cursor.moveToFirst()) {
            do {
                receivedHoras.add(DataMenuHoras(cursor.getString(cursor.getColumnIndex(FechaContract.FechaEntrada.INICIO)),
                        cursor.getString(cursor.getColumnIndex(FechaContract.FechaEntrada.FINAL))))
                //receivedHoras.setImage(cursor.getString(cursor.getColumnIndex(COLUMN_PERSON_IMAGE)))
            }while(cursor.moveToNext())
        }
        cursor.close()
        return receivedHoras
    }
    fun insertUsuario(Usuario:String,Password:String){
        val db  = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(FechaContract.FechaEntrada.USER, Usuario)
        contentValues.put(FechaContract.FechaEntrada.PASSWORD, Password)
        db.insert(FechaContract.FechaEntrada.USERDATA, null, contentValues)
    }
    fun insertHora(horas:menuConfig, evento:String)
    {
        val db  = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(FechaContract.FechaEntrada.INICIO, horas.horaInicio)
        contentValues.put(FechaContract.FechaEntrada.FINAL, horas.horafinal)
        contentValues.put(FechaContract.FechaEntrada.EVENTO,evento)
        db.insert(horas.Dia, null, contentValues)
    }
    fun insertParametros(parametroNotificacion: Boolean, parametroNumeroSMS: String){
        val db = writableDatabase
        val contentValues = ContentValues()
        contentValues.put(FechaContract.FechaEntrada.NOTIFICATION, if (parametroNotificacion) 1 else 0)
        contentValues.put(FechaContract.FechaEntrada.NUMEROSMS, parametroNumeroSMS)
        db.insert(FechaContract.FechaEntrada.USERPARAMETERS, null, contentValues)
    }
    fun updateParametroNumero(parametroNumeroSMS: String){
        val db = writableDatabase
        db.execSQL("UPDATE  ${FechaContract.FechaEntrada.USERPARAMETERS} " +
                "SET ${FechaContract.FechaEntrada.NUMEROSMS} ='${parametroNumeroSMS}'" +
                "WHERE ${FechaContract.FechaEntrada.ID}= 1 ")
    }
    fun updateParametroNotificacion(parametroNotificacion: Boolean){
        val db = writableDatabase
        db.execSQL("UPDATE  ${FechaContract.FechaEntrada.USERPARAMETERS} " +
                "SET ${FechaContract.FechaEntrada.NOTIFICATION} =${if (parametroNotificacion) 1 else 0} " +
                "WHERE ${FechaContract.FechaEntrada.ID}= 1 ")
    }
    fun updateHorasRecord(horas:DataMenuHoras, updatedHoras: DataMenuHoras,dia:String) {
        val db = this.writableDatabase
        //you can use the constants above instead of typing the column names
        db.execSQL("UPDATE  $dia SET ${FechaContract.FechaEntrada.INICIO} ='${horas.horaInicio}', ${FechaContract.FechaEntrada.FINAL} = '${horas.horafinal}'" +
                "WHERE ${FechaContract.FechaEntrada.INICIO}= '${updatedHoras.horaInicio}' AND ${FechaContract.FechaEntrada.FINAL} = '${updatedHoras.horafinal}'")

    }

    fun deleteHorasRecord(horas:DataMenuHoras, dia:String) {
        val db = this.writableDatabase
        db.execSQL("DELETE FROM $dia WHERE ${FechaContract.FechaEntrada.INICIO}= '${horas.horaInicio}'" +
                "AND ${FechaContract.FechaEntrada.FINAL} = '${horas.horafinal}'")
    }
    fun getEvento(horas:DataMenuHoras,dia:String): String {
        val db = this.readableDatabase
        var subQuery1 = ""
        var subQuery2 = ""
        var and = ""
        if(horas.horaInicio != "NA"){
            subQuery1 = " ${FechaContract.FechaEntrada.INICIO} = '${horas.horaInicio}'"
            and = " AND"
        }
        if(horas.horafinal != "NA")
            subQuery2 = "$and ${FechaContract.FechaEntrada.FINAL} = '${horas.horafinal}'"
        val query = "SELECT ${FechaContract.FechaEntrada.EVENTO} FROM $dia WHERE $subQuery1 $subQuery2"
        val cursor = db.rawQuery(query, null)
        try {
            if (cursor.moveToFirst()) {
                val evento = cursor.getString(cursor.getColumnIndex(FechaContract.FechaEntrada.EVENTO))
                return if(evento.length > 0) evento else ""
            } else return ""
        }catch (ex:Exception){
            Toast.makeText(context,ex.message,Toast.LENGTH_LONG).show()
            return ""
        }
        finally{
            cursor.close()}
    }
    fun getParametros():MutableList<String>{
        val db = this.readableDatabase
        var parametros: MutableList<String> = ArrayList()
        val query = "SELECT * FROM ${FechaContract.FechaEntrada.USERPARAMETERS}"
        val cursor =  db.rawQuery(query,null)
        try {
            if (cursor.moveToFirst()) {
                parametros.add(cursor.getString(cursor.getColumnIndex(FechaContract.FechaEntrada.NOTIFICATION)))
                parametros.add(cursor.getString(cursor.getColumnIndex(FechaContract.FechaEntrada.NUMEROSMS)))
            }
            return parametros
        }catch (ex:Exception){
            Toast.makeText(context,ex.message,Toast.LENGTH_LONG).show()
            TODO(ex.message.toString())
        }
        finally{
            cursor.close()}
    }
    //endregion
 //region Validaciones
    fun existe(horas:DataMenuHoras,dia:String): Boolean {
        val db = this.readableDatabase
        var subQuery1 = ""
        var subQuery2 = ""
        var and = ""
        if(horas.horaInicio != "NA"){
            subQuery1 = " WHERE ${FechaContract.FechaEntrada.INICIO} = '${horas.horaInicio}'"
            and = " AND"
        }
        if(horas.horafinal != "NA")
            subQuery2 = "$and ${FechaContract.FechaEntrada.FINAL} = '${horas.horafinal}'"
        val query = "SELECT * FROM $dia $subQuery1 $subQuery2"
        val cursor = db.rawQuery(query, null)
        return if (cursor.count > 0){
            cursor.close()
            true
        }
        else{
            cursor.close()
            false
        }
    }
    fun hayConflicto(valor1:Int,valor2:Int,dia:String): Boolean{
        val db = this.readableDatabase
        val query = "SELECT CAST(substr(${FechaContract.FechaEntrada.INICIO},1,2) || substr(${FechaContract.FechaEntrada.INICIO},4,2) AS INTEGER) AS HInicio," +
                " CAST(substr(${FechaContract.FechaEntrada.FINAL},1,2) || substr(${FechaContract.FechaEntrada.FINAL},4,2) AS INTEGER) AS HFinal" +
                " FROM $dia WHERE  HInicio BETWEEN $valor1 AND $valor2" +
                " OR HFinal BETWEEN $valor1 AND $valor2" +
                " OR ( HInicio <= $valor1 AND HFinal >= $valor2 )"
        try {
            val cursor = db.rawQuery(query, null)
            return if (cursor.count > 0) {
                cursor.close()
                true
            } else {
                cursor.close()
                false
            }
        }
        catch (ex:Exception){
                Toast.makeText(context,ex.message,Toast.LENGTH_LONG).show()
                return true
        }

    }
    fun validaUsuario(usuario:String, contrasena:String):Boolean{
        val db = this.readableDatabase
        val query = "SELECT * FROM ${FechaContract.FechaEntrada.USERDATA} " +
                "WHERE  ${FechaContract.FechaEntrada.USER} = $usuario " +
                "AND  ${FechaContract.FechaEntrada.PASSWORD} = $contrasena)"
        return try {
            val cursor = db.rawQuery(query, null)
            cursor.close()
            (cursor.count > 0)
        }
        catch (ex:Exception){
            Toast.makeText(context,ex.message,Toast.LENGTH_LONG).show()
            false
        }
    }
    fun existenUsuarios():Boolean{
        val db = this.readableDatabase
        val query = "SELECT * FROM ${FechaContract.FechaEntrada.USERDATA} "
        try {
            val cursor = db.rawQuery(query, null)
            return if (cursor.count > 0) {
                cursor.close()
                true
            } else {
                cursor.close()
                false
            }
        }
        catch (ex:Exception){
            Toast.makeText(context,ex.message,Toast.LENGTH_LONG).show()
            return false
        }
    }
    //endregion
}