package com.example.romul.appbomba

import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_config_hora.*
import android.app.TimePickerDialog
import android.text.InputType
import android.widget.Toast
import android.support.v7.widget.Toolbar
import android.telephony.SmsManager
import java.util.*
import android.content.pm.PackageManager
import android.view.View
import android.view.View.STATUS_BAR_HIDDEN
import android.view.View.SYSTEM_UI_FLAG_FULLSCREEN
import android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
class ConfigHora : AppCompatActivity() {
    var dia:String? = null
    var evento:String? = null
    var horaInicio:String? = null
    var horaFinal:String?  = null
    var id:String? = null
    var createNew:Boolean? = null
    companion object {
        val CERO = "0"
        val DOS_PUNTOS = ":"
        val c = Calendar.getInstance()
        val hora = c.get(Calendar.HOUR_OF_DAY)
        val minuto = c.get(Calendar.MINUTE)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_config_hora)
        val bundle = intent.extras
        dia = bundle.getString("Tabla")
        evento = bundle.getString("NombreEvento")
        horaInicio = bundle.getString("Hora Inicial")
        horaFinal = bundle.getString("Hora Final")
        id = bundle.getString("id")
        createNew = bundle.getBoolean("Crear Row")

        Cinicial.setText(nullToString(horaInicio))
        Cfinal.setText(nullToString(horaFinal))
        if(createNew == false) {
            CeventName.setInputType(InputType.TYPE_NULL)
            Cagregar.setText("Modificar")
        }
        Cagregar.setOnClickListener {
            if(EsValido()) {
                if (createNew == false)
                    Modificar()
                else
                    Agregar()
                Ccancelar.setText("Salir")
            }
        }
        Ccancelar.setOnClickListener {Salir()}
        Einicio.setOnClickListener {  MostrarTimePicker(true)}
        Efinal.setOnClickListener { MostrarTimePicker(false)}
        //region ValidacionesEditText
        Cinicial.setInputType(InputType.TYPE_NULL)
        Cfinal.setInputType(InputType.TYPE_NULL)
        CeventName.setOnFocusChangeListener { v, hasFocus ->
            if(!hasFocus){
                CeventName.setInputType(InputType.TYPE_NULL)
            }
        }
        //endregion
    }
    private fun Agregar(){
        val db = FechaDbHelper(this)
        val sms = MetodosParaSMS(this)
        horaInicio = Cinicial.text.toString()
        horaFinal = Cfinal.text.toString()
        evento = CeventName.text.toString()
        val data = menuConfig(dia.toString(),horaInicio.toString(),horaFinal.toString())
        db.insertHora(data,if (evento.isNullOrEmpty()) "" else evento.toString())
        Cinicial.setText("")
        Cfinal.setText("")
        CeventName.setText("")
        sms.enviarSMS_Insertar("$dia-$horaInicio-$horaFinal")
    }
    private fun Modificar(){
        val db = FechaDbHelper(this)
        val sms = MetodosParaSMS(this)
        if((horaInicio != null) and (horaFinal != null) and (dia != null)) {
            val newDatos = DataMenuHoras(Cinicial.text.toString(),Cfinal.text.toString())
            val oldDatos = DataMenuHoras(horaInicio.toString(), horaFinal.toString())
            db.updateHorasRecord(newDatos,oldDatos,dia.toString())
            sms.enviarSMS_Modificar("$dia-$horaInicio-$horaFinal")
        }
        Salir()
    }
    private fun Salir(){
        onNavigateUp()
    }
    private fun MostrarTimePicker(valor:Boolean){
        val recogerHora = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
            //Formateo el hora obtenido: antepone el 0 si son menores de 10
            val horaFormateada = if (hourOfDay < 10) CERO + hourOfDay else hourOfDay.toString()
            //Formateo el minuto obtenido: antepone el 0 si son menores de 10
            val minutoFormateado = if (minute < 10) CERO + minute else minute.toString()
            //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
            val AM_PM: String
            if (hourOfDay < 12) {
                AM_PM = "a.m."
            } else {
                AM_PM = "p.m."
            }
            //Muestro la hora con el formato deseado
            if(valor)
               Cinicial.setText("$horaFormateada$DOS_PUNTOS$minutoFormateado $AM_PM")
            else
               Cfinal.setText("$horaFormateada$DOS_PUNTOS$minutoFormateado $AM_PM")
        },hora, minuto, true)

        recogerHora.show()
    }
    private fun EsValido(): Boolean {
        val fecha1 = Cinicial.text.toString()
        val fecha2= Cfinal.text.toString()
        val valor1 = fecha1.removeRange(2,3).substring(0,4).toInt()
        val valor2 = fecha2.removeRange(2,3).substring(0,4).toInt()
        val db = FechaDbHelper(this)
         if(fecha1 == "" || fecha2 == ""){
            Toast.makeText(this,"Debe ingresar una hora de inicio y final",Toast.LENGTH_LONG).show()
            return false
        }

        else if(valor1 > valor2)
        {
            Toast.makeText(this,"La hora de inicio no debe ser mayor",Toast.LENGTH_LONG).show()
            return false
        }
        else if(db.existe(DataMenuHoras(fecha1,fecha2),dia.toString())){
            Toast.makeText(this,"Este evento ya existe",Toast.LENGTH_LONG).show()
            return false
        }
        else if(db.hayConflicto(valor1,valor2,dia.toString())){
             Toast.makeText(this,"Se hallaron conflictos con otro evento existente",Toast.LENGTH_LONG).show()
             return false
         }
        else
            return true
    }
    private fun nullToString(dato:String?):String{
        return if(dato != null)  dato.toString() else ""
    }
    override fun onBackPressed() {
        finish()
    }
}
