package com.example.romul.appbomba

import android.app.IntentService
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.PendingIntent.getActivity
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.support.v4.app.NotificationCompat
import android.widget.Toast
import java.util.*

class ServiceAppBomba: IntentService("ServiceAppBomba") {
    companion object {
        val CHANNEL_ID = 1
        val LUNES = "Lunes"
        val MARTES = "Martes"
        val MIERCOLES = "Miercoles"
        val JUEVES = "Jueves"
        val VIERNES = "Viernes"
        val SABADO = "Sabado"
        val DOMINGO = "Domingo"
    }
    private var ultimaHora:String? = null
    private var esInicioOFinal = false
    var evento =""
    var dia=""
    override fun onCreate() {
        super.onCreate()
    }
    override fun onDestroy() {
        super.onDestroy()
    }
    override fun onHandleIntent(intent: Intent?) {
        try{
            while(true) {
                val dateFormat = Calendar.getInstance()
                val horaActual = getHoraActual(dateFormat)
                Toast.makeText(this,horaActual,Toast.LENGTH_SHORT).show()
                if(ultimaHora != horaActual) {
                    if (coincideFechaActual(dateFormat, horaActual)) {
                        GenerateNotification(
                                if (!esInicioOFinal) "La bomba se ha encendido para el evento: $evento"
                                else "La bomba se ha apago para el evento: $evento")
                    }
                }
            }
        } catch (ex:Exception){
            Toast.makeText(applicationContext,ex.message,Toast.LENGTH_LONG).show()
        }
    }
    fun getHoraActual(dateFormat:Calendar): String{
        val ValH = dateFormat.get(Calendar.HOUR_OF_DAY)
        val ValM = dateFormat.get(Calendar.MINUTE)
        val _horaActual = if(ValH<10) "0$ValH" else ValH.toString()
        val _minActual = if(ValM<10) "0$ValM" else ValM.toString()
        val AM_PM = if( ValH < 12) "a.m." else "p.m."
        return "$_horaActual:$_minActual $AM_PM"
    }
    private fun GenerateNotification(text: String) {
        try {
            Toast.makeText(this,"notify",4000.toInt()).show()
            val intent = Intent(this, MenuHoras::class.java)
            intent.putExtra("dia", dia)
            val pending = PendingIntent.getActivity(this, 1, intent, 0)
            val mBuilder = NotificationCompat.Builder(this)
                    .setContentIntent(pending)
                    .setAutoCancel(true)
                    .setContentTitle("Control Remoto de Bomba")
                    .setContentText(text)
                    .setSmallIcon(R.drawable.bombalogo)
            val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.notify(CHANNEL_ID, mBuilder.build())
        }catch (ex:Exception){Toast.makeText(this,ex.message,Toast.LENGTH_LONG).show()}
    }
    private fun coincideFechaActual(formatoFecha:Calendar,hora:String):Boolean{

        dia = getDay(formatoFecha.get(Calendar.DAY_OF_WEEK))
        val db = FechaDbHelper(this)
        fun buscaHora(inicioFinal:Boolean):Boolean{
            val horas = if(!inicioFinal)DataMenuHoras("NA",hora) else DataMenuHoras(hora,"NA")
            evento = db.getEvento(horas,dia)
            esInicioOFinal = !inicioFinal
            return if(evento.isNotEmpty()){
                ultimaHora = hora
                true
            }else false
        }
        return if(buscaHora(false)) true; else buscaHora(true)
    }
    private fun getDay(num:Int):String{
        return when(num) {
            1 -> DOMINGO
            2-> LUNES
            3 -> MARTES
            4-> MIERCOLES
            5 -> JUEVES
            6 -> VIERNES
            7 -> SABADO
            else -> ""
        }

    }
}