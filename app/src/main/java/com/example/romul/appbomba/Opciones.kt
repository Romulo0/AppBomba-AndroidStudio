package com.example.romul.appbomba

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar

class Opciones : AppCompatActivity() {
    lateinit var mRecyclerView : RecyclerView
    lateinit var mAdapter : RecyclerOpciones
    lateinit var valParametros:MutableList<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_opciones)
        val toolBar: Toolbar = findViewById(R.id._toolbarOpciones)
        setSupportActionBar(toolBar)
        toolBar.setNavigationOnClickListener{
            finish()
        }
        setUpRecyclerView()

    }
    fun getDbValues(){
        val db = FechaDbHelper(this)
        valParametros = db.getParametros()
        valParametros.add("")
    }

    private fun setUpRecyclerView(){
        getDbValues()
        val _opciones = mutableListOf<DataMenuOpciones>()
        _opciones.add(DataMenuOpciones(true, "Activar/Desactivar notificaciones"))
        _opciones.add(DataMenuOpciones(true, "Activar/Desactivar Cuenta de Google"))
        _opciones.add(DataMenuOpciones(false, "Seleccionar Numero para envio de SMS"))
        mAdapter = RecyclerOpciones(_opciones, this,valParametros)
        mRecyclerView = findViewById(R.id.Olista)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        val itemDecorator = RecyclerDeviderDecoration()
        itemDecorator.DividerItemDecoration(this)
        mRecyclerView.addItemDecoration(itemDecorator)
        mRecyclerView.adapter = mAdapter
    }
}

