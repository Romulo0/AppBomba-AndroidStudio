package com.example.romul.appbomba

import android.app.Activity
import android.app.FragmentManager
import android.content.Context
import android.content.Intent
import android.support.v4.widget.CompoundButtonCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.view.menu.MenuView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast
class RecyclerOpciones( val _valOpciones:MutableList<DataMenuOpciones>, val _context:Context, val _valParameters:MutableList<String>
): RecyclerView.Adapter<RecyclerOpciones.ViewHolderOpciones>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderOpciones {
        val layoutInflater= LayoutInflater.from(parent.context)
        return ViewHolderOpciones(layoutInflater.inflate(R.layout.recycleropciones,parent,false))
    }

    override fun getItemCount(): Int {
       return _valOpciones.size
    }

    override fun onBindViewHolder(holder: ViewHolderOpciones, position: Int) {
        holder.bind(_context,position,_valOpciones[position], _valParameters)
    }
    inner class ViewHolderOpciones(view:View): RecyclerView.ViewHolder(view){
        var switchHolder = view.findViewById<Switch>(R.id.switchRecyclerOpciones)
        var textHolder = view.findViewById<TextView>(R.id.textRecyclerOpciones)
        fun bind(context:Context,position:Int,data: DataMenuOpciones, parametro:MutableList<String>){
            textHolder.text = data.texto
             switchHolder.visibility = if (data.activoInactivo)
                 View.VISIBLE
             else
                 View.INVISIBLE
             when (position){
                 0-> {
                     switchHolder.isChecked = if(parametro[0] == "1") true else false
                     switchHolder.setOnClickListener {
                         try {
                             val db = FechaDbHelper(_context)
                             if((it as Switch).isChecked){
                                 val intent = Intent(context, ServiceAppBomba::class.java)
                                 context.startService(intent)
                                 db.updateParametroNotificacion(true)
                                 Toast.makeText(context,"Notificacion Activada", Toast.LENGTH_SHORT).show()
                            }
                            else{
                                 val intent = Intent(context,ServiceAppBomba::class.java)
                                 context.stopService(intent)
                                 db.updateParametroNotificacion(false)
                                 Toast.makeText(context,"Notificacion Desactivada",Toast.LENGTH_SHORT).show()
                            }
                         }catch (ex:Exception){
                            Toast.makeText(context,"Error:${ex.message}",Toast.LENGTH_LONG).show()
                        }
                     }
                 }
                 2 ->{
                     itemView.setOnClickListener {
                         try {
                             val db = FechaDbHelper(_context)
                             val intent = Intent(_context,smsOpciones::class.java)
                             intent.putExtra("Numero",db.getParametros()[1])
                             _context.startActivity(intent)
                         } catch (ex:Exception){
                             Toast.makeText(_context,ex.message, Toast.LENGTH_LONG).show()
                         }
                     }
                 }
             }
        }
    }
}