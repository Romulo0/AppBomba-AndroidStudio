package com.example.romul.appbomba

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.PatternMatcher
import android.provider.CalendarContract
import android.support.design.internal.NavigationMenu
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.iniciosesion.*
import java.util.regex.Pattern

class InicioSesion:AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.iniciosesion)
        val btnIniciarSesion = findViewById<Button>(R.id.inicioSesion)
        btnIniciarSesion.setOnClickListener {
            try {
                val db = FechaDbHelper(this)
                if (db.validaUsuario(txtUsuario.text.toString(), txtContrasena.text.toString())) {
                    val intent = Intent(this, MenuHoras::class.java)
                    startActivity(intent)
                } else {
                    Toast.makeText(this, "Usuario Invalido", Toast.LENGTH_SHORT).show()
                }
            }catch (ex:Exception){
                Toast.makeText(this,ex.message,Toast.LENGTH_LONG).show()
            }
        }
    }
    override fun onBackPressed() {
       finish()
    }
}