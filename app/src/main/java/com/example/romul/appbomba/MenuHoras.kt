package com.example.romul.appbomba

import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabItem
import android.support.design.widget.TabLayout
import android.support.v4.app.NavUtils
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import kotlinx.android.synthetic.main.activity_menu_horas.*
import android.support.design.widget.TabLayout.OnTabSelectedListener
import android.widget.Toast


class MenuHoras : AppCompatActivity() {
    lateinit var mRecyclerView : RecyclerView
    lateinit var mAdapter : RecyclerHoras
    var dia:String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_horas)
        dia = "Lunes"
        mAdapter = RecyclerHoras()
        setUpRecyclerView(dia)
        agregar.setOnClickListener{
            val intent = Intent(this,ConfigHora::class.java)
            intent.putExtra("Crear Row", true)
            intent.putExtra("Tabla",dia)
            startActivity(intent)
        }
        btnOpciones.setOnClickListener {
            val intent = Intent(this, Opciones::class.java)
            startActivity(intent)
        }
        btnInterfazControl.setOnClickListener {
            val intent = Intent(this, InterfazDeControl::class.java)
            startActivity(intent)
        }
        val toolBar: Toolbar = findViewById(R.id._toolbar)
        setSupportActionBar(toolBar)
        initializeTabs()
    }
    /* override fun onCreateOptionsMenu(menu: Menu): Boolean {
         menuInflater.inflate(R.menu.main_menu, menu)
         return true
     }
     override  fun onOptionsItemSelected( item: MenuItem): Boolean
     {
         return when (item.getItemId()){
                 android.R.id.home ->{
                     NavUtils.navigateUpFromSameTask(this)
                     true
                 }
                 R.id.ActivarNotify -> {
                     try {
                         val intent = Intent(this, ServiceAppBomba::class.java)
                         startService(intent)
                         Toast.makeText(this,"Notificacion Activada",Toast.LENGTH_LONG).show()
                     }catch(ex:Exception){
                         Toast.makeText(this,"Error:${ex.message}",Toast.LENGTH_LONG).show()
                     }

                     true
                 }
                 R.id.DesactivarNotify ->{
                     try {
                         val intent = Intent(this,ServiceAppBomba::class.java)
                         stopService(intent)
                         Toast.makeText(this,"Notificacion Desactivada",Toast.LENGTH_LONG).show()
                     }catch (ex:Exception){
                         Toast.makeText(this,"Error:${ex.message}",Toast.LENGTH_LONG).show()
                     }
                     true
                 }
                 else -> super.onOptionsItemSelected(item)
         }
     }*/
    private fun initializeTabs(){
        tabs.addTab(tabs.newTab().setText("Lunes"))
        tabs.addTab(tabs.newTab().setText("Martes"))
        tabs.addTab(tabs.newTab().setText("Miercoles"))
        tabs.addTab(tabs.newTab().setText("Jueves"))
        tabs.addTab(tabs.newTab().setText("Viernes"))
        tabs.addTab(tabs.newTab().setText("Sabado"))
        tabs.addTab(tabs.newTab().setText("Domingo"))
        tabs.addOnTabSelectedListener(object :TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                when(tab?.position){
                    0 ->{dia = "Lunes"
                        mAdapter = RecyclerHoras()
                        setUpRecyclerView(dia)
                    }
                    1 ->{dia = "Martes"
                        mAdapter = RecyclerHoras()
                        setUpRecyclerView(dia)
                    }
                    2 ->{ dia = "Miercoles"
                        mAdapter = RecyclerHoras()
                        setUpRecyclerView(dia)}
                    3 ->{ dia = "Jueves"
                        mAdapter = RecyclerHoras()
                        setUpRecyclerView(dia)}
                    4 ->{ dia = "Viernes"
                        mAdapter = RecyclerHoras()
                        setUpRecyclerView(dia)}
                    5 ->{ dia = "Sabado"
                        mAdapter = RecyclerHoras()
                        setUpRecyclerView(dia)}
                    6 ->{dia = "Domingo"
                        mAdapter = RecyclerHoras()
                        setUpRecyclerView(dia)
                    }
                }
            }
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }
        })
    }
    private fun setUpRecyclerView(dia:String){
        mRecyclerView = findViewById(R.id.Hlista)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        val itemDecorator = RecyclerDeviderDecoration()
        itemDecorator.DividerItemDecoration(this)
        mRecyclerView.addItemDecoration(itemDecorator)
        mAdapter.RecyclerHoras(ejecutaBusqueda(dia), this, dia)
        mRecyclerView.adapter = mAdapter
    }
    private fun ejecutaBusqueda(_dia:String):MutableList<DataMenuHoras>{
        var horas:MutableList<DataMenuHoras> = ArrayList()
        var db = FechaDbHelper(this)
        horas = db.getHoras(_dia)
        return horas
    }

    override fun onBackPressed() {
    }
}
