package com.example.romul.appbomba

import android.app.AlertDialog
import android.app.PendingIntent.getActivity
import android.content.Context
import android.content.DialogInterface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.widget.Toast
import java.lang.reflect.Array


class RecyclerHoras : RecyclerView.Adapter<RecyclerHoras.viewHolderHoras>() {
    var horas : MutableList<DataMenuHoras> = ArrayList()
    lateinit var tableName:String
    lateinit var context : Context
    fun RecyclerHoras(_opciones : MutableList<DataMenuHoras>,_context : Context,_tableName:String){
        horas = _opciones
        context = _context
        tableName = _tableName
    }
    override fun onBindViewHolder(holder: viewHolderHoras, position: Int) {
        val item = horas.get(position)
        holder.bind(item,context, position, tableName,horas)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolderHoras {
        val layoutInflater = LayoutInflater.from(parent.context)
        return viewHolderHoras(layoutInflater.inflate(R.layout.recyclerlayout_horas ,parent,false))
    }
    override fun getItemCount(): Int{
        return horas.size
    }
    inner class viewHolderHoras(view: View) : RecyclerView.ViewHolder(view){
        var horai: TextView = view.findViewById(R.id.Hinicio)
        var horaf: TextView = view.findViewById(R.id.Hfinal)
        var evento: TextView = view.findViewById(R.id.Hevento)
        lateinit var dataHolder : DataMenuHoras
        fun bind (menu:DataMenuHoras,context: Context, position:Int, tableName:String,dataSet: MutableList<DataMenuHoras>){
            val eventVal = getNombreEvento(tableName,menu)
            val tablaHolder = tableName
            horai.text = menu.horaInicio
            horaf.text = menu.horafinal
            evento.text = eventVal
            dataHolder = menu
            itemView.setOnClickListener (View.OnClickListener {
                val intent = Intent(context,ConfigHora::class.java)
                intent.putExtra("NombreEvento",eventVal)
                intent.putExtra("Hora Inicial",dataHolder.horaInicio)
                intent.putExtra("Hora Final",dataHolder.horafinal)
                intent.putExtra("id",position)
                intent.putExtra("Tabla",tablaHolder)
                intent.putExtra("Crear Row", false)
                ContextCompat.startActivity(context, intent, null)
                
            })
            itemView.setOnLongClickListener {
                dataSet.removeAt(position)
                eliminarGrupoDeTextDate(dataHolder,tablaHolder,context)
            }
        }
        private fun eliminarGrupoDeTextDate(horas:DataMenuHoras,dia:String,context: Context):Boolean{
            val db = FechaDbHelper(context)
            val sms = MetodosParaSMS(context)
            val builder = AlertDialog.Builder(context)
            builder.setTitle("Mensaje de alerta")
                    .setMessage("¿Está seguro de eliminar el registro?")
                    .setPositiveButton("Si, deseo eliminarlo",fun(_,_) {
                            db.deleteHorasRecord(horas, dia)
                            sms.enviarSMS_Eliminar("$dia-${horas.horaInicio}-${horas.horafinal}")
                            notifyDataSetChanged()
                        }
                    )
                    .setNeutralButton("No, me equivoque", fun(_,_) {
                            Toast.makeText(context,"Operacion cancelada",Toast.LENGTH_LONG).show()
                        }
                    )
            builder.show()
            return true
        }
        private fun getNombreEvento(dia:String,horas: DataMenuHoras):String{
            var evento = ""
            var db = FechaDbHelper(context)
            evento = db.getEvento(horas,dia)
            return evento
        }
    }
}

