package com.example.romul.appbomba

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View
import android.opengl.ETC1.getWidth
import android.graphics.drawable.Drawable
import android.content.res.TypedArray
import android.support.v4.content.ContextCompat


class RecyclerDeviderDecoration: RecyclerView.ItemDecoration(){

    lateinit var divider: Drawable
    private val ATTRS = intArrayOf(android.R.attr.listDivider)

    fun DividerItemDecoration(context: Context) {
        val styledAttributes = context.obtainStyledAttributes(ATTRS)
        divider = styledAttributes.getDrawable(0)
        styledAttributes.recycle()
    }

    fun DividerItemDecoration(context: Context, resId: Int){
        divider = ContextCompat.getDrawable(context, resId)
    }
    override fun onDraw(c: Canvas?, parent: RecyclerView, state: RecyclerView.State?) {
        super.onDraw(c, parent, state)
        val left = parent.getPaddingLeft()
        val right = parent.getWidth() - parent.getPaddingRight()

        val childCount = parent.getChildCount()
        for (i in 0 until childCount) {
            val child = parent.getChildAt(i)

            val params = child.layoutParams as RecyclerView.LayoutParams

            val top = child.bottom + params.bottomMargin
            val bottom = top + divider.getIntrinsicHeight()

            divider.setBounds(left, top, right, bottom)
            divider.draw(c)
        }
    }

    override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {
        super.getItemOffsets(outRect, view, parent, state)
    }
}
