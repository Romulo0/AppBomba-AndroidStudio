package com.example.romul.appbomba

import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import java.util.regex.Pattern

class Registrarse : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registrarse)
        val btnRegistrarse = findViewById<Button>(R.id.btnRegistrarse)
        val txtContrasena = findViewById<TextView>(R.id.txtContrasena)
        val txtContrasena2 = findViewById<TextView>(R.id.txtContrasena2)
        var coincide:Boolean
        txtContrasena.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                coincide = Pattern.matches("^(?=\\w*[a-z])(?=\\w*[A-Z])(?=\\w*\\d)([^ ]){8,15}\$",s.toString())
                if(!coincide){txtContrasena.setTextColor(Color.RED)
                }else{ txtContrasena.setTextColor(Color.BLACK)}
            }
        })
        btnRegistrarse.setOnClickListener {
            if(txtContrasena.text.toString() != txtContrasena2.text.toString()){
                Toast.makeText(this,"La contraseña no coincide",Toast.LENGTH_LONG).show()
            }else {
                val intent = Intent(this, InicioSesion::class.java)
                startActivity(intent)
                finish()
            }
        }

    }
    override fun onBackPressed() {
        finish()
    }
}
