package com.example.romul.appbomba

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityCompat.startActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import android.support.v4.app.ActivityCompat.startActivityForResult
import android.support.v4.app.NavUtils
import android.support.v4.content.ContextCompat
import android.widget.Button
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import android.widget.Toast
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.SignInButton
import com.google.android.gms.tasks.Task
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnCompleteListener
import kotlinx.android.synthetic.main.activity_escoger_iniciar_oregistrar.*


class EscogerIniciarORegistrar : AppCompatActivity() {

    companion object{
        val RC_SIGN_IN = 200
    }
    var conectado = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_escoger_iniciar_oregistrar)
        val btnSignIn = findViewById<SignInButton>(R.id.sign_in_button)
        val btnSignInLocal = findViewById<Button>(R.id.signInLocal)
        val btnRegistrarseInLocal = findViewById<Button>(R.id.registrarseInLocal)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        val mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        btnSignIn.setOnClickListener{
            try {
                if (conectado) {
                    mGoogleSignInClient.signOut().addOnCompleteListener(this, {
                        Toast.makeText(this,"Desconectado",Toast.LENGTH_SHORT).show()
                    })
                    btnSignInLocal.text = "INICIAR SESIÓN"
                    conectado = false
                } else {
                    startActivityForResult(mGoogleSignInClient.signInIntent, RC_SIGN_IN)
                }
            }catch (ex:Exception){
                Toast.makeText(this,ex.message,Toast.LENGTH_LONG).show()
            }
        }
        btnSignInLocal.setOnClickListener{
            if(conectado){
               val intent = Intent(this, MenuHoras::class.java)
                startActivity(this, intent, null)
            }
            else {
                val intent = Intent(this, InicioSesion::class.java)
                startActivity(this, intent, null)
            }
        }
        btnRegistrarseInLocal.setOnClickListener{
            val intent = Intent(this, Registrarse::class.java)
            startActivity(this,intent,null)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (requestCode == RC_SIGN_IN) {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                handleSignInResult(task, this)
            }
        }catch (ex:Exception){
            Toast.makeText(this,ex.message,Toast.LENGTH_LONG).show()
        }
    }

    override fun onStart() {
        super.onStart()
        val account = GoogleSignIn.getLastSignedInAccount(this)
        val btnSignInLocal = findViewById<Button>(R.id.signInLocal)
        conectado = if(account != null){
            btnSignInLocal.text = "COMENZAR"
            true
        }else {
            btnSignInLocal.text = "INICIAR SESIÓN"
            false
        }
    }
    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>,context: Context) {
        try {
            // Signed in successfully, show authenticated UI.
            val account = completedTask.getResult(ApiException::class.java)
            val db = FechaDbHelper(context)
            db.insertUsuario(account.id.toString(),account.idToken.toString())
            val intent = Intent(context,MenuHoras::class.java)
            ContextCompat.startActivity(context,intent,null)
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Toast.makeText(context, "signInResult:failed code=" + e.statusCode, Toast.LENGTH_LONG).show()
            //updateUI(null)
        }

    }

}


