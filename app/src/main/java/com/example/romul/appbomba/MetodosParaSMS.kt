package com.example.romul.appbomba

import android.content.Context
import android.content.pm.PackageManager
import android.telephony.SmsManager
import android.widget.Toast


class MetodosParaSMS(val context:Context){
    companion object {
        val ON = "ON"
        val OFF = "OFF"
        val SEPARADOR = "-"
        val INSERTAR = 1
        val MODIFICAR = 2
        val ELIMINAR = 3
        val INICIOTRAMA = "$"
    }
    fun enviarSMS_Insertar(message: String){
        try {
            val pm = context.packageManager

            if (!pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY) && !pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY_CDMA)) {
                Toast.makeText(context, "Lo sentimos, tu dispositivo probablemente no pueda enviar SMS...", Toast.LENGTH_LONG).show()
                return
            }
            val db = FechaDbHelper(context)
            val num = db.getParametros()[1]
            val sms = SmsManager.getDefault()
            sms.sendTextMessage(num, null,"$INICIOTRAMA$message$SEPARADOR$INSERTAR", null, null)
            Toast.makeText(context, "Sent.", Toast.LENGTH_LONG).show()
        }
        catch(ex:Exception){
            TODO(ex.message.toString())
        }
    }
    fun enviarSMS_Modificar(message: String){
        try {
            val pm = context.packageManager

            if (!pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY) && !pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY_CDMA)) {
                Toast.makeText(context, "Lo sentimos, tu dispositivo probablemente no pueda enviar SMS...", Toast.LENGTH_LONG).show()
                return
            }
            val db = FechaDbHelper(context)
            val num = db.getParametros()[1]
            val sms = SmsManager.getDefault()
            sms.sendTextMessage(num, null,"$INICIOTRAMA$message$SEPARADOR$MODIFICAR", null, null)
            Toast.makeText(context, "Sent.", Toast.LENGTH_LONG).show()
        }
        catch(ex:Exception){
            TODO(ex.message.toString())
        }
    }
    fun enviarSMS_Eliminar(message: String){
        try {
            val pm = context.packageManager

            if (!pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY) && !pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY_CDMA)) {
                Toast.makeText(context, "Lo sentimos, tu dispositivo probablemente no pueda enviar SMS...", Toast.LENGTH_LONG).show()
                return
            }
            val db = FechaDbHelper(context)
            val num = db.getParametros()[1]
            val sms = SmsManager.getDefault()
            sms.sendTextMessage(num, null,"$INICIOTRAMA$message$SEPARADOR$ELIMINAR", null, null)
            Toast.makeText(context, "Sent.", Toast.LENGTH_LONG).show()
        }
        catch(ex:Exception){
            TODO(ex.message.toString())
        }
    }
    fun enviarSMS_EncenderOApagar(comando:Boolean){
        try {
            val pm = context.packageManager

            if (!pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY) && !pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY_CDMA)) {
                Toast.makeText(context, "Lo sentimos, tu dispositivo probablemente no pueda enviar SMS...", Toast.LENGTH_LONG).show()
                return
            }
            val db = FechaDbHelper(context)
            val num = db.getParametros()[1]
            val sms = SmsManager.getDefault()
            sms.sendTextMessage(num, null,"$INICIOTRAMA${if(comando) ON else OFF}", null, null)
        }
        catch(ex:Exception){
            TODO(ex.message.toString())
        }
    }
}