package com.example.romul.appbomba

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class smsOpciones : AppCompatActivity() {
    lateinit var strNumero:String
    lateinit var btnAceptar:Button
    lateinit var btnCancelar:Button
    lateinit var txtNumero1:EditText
    lateinit var txtNumero2:EditText
    lateinit var txtNumero3:EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sms_opciones)
        btnAceptar = findViewById<Button>(R.id.btnAceptarOpciones)
        btnCancelar = findViewById<Button>(R.id.btnCancelarOpciones)
        txtNumero1 = findViewById<EditText>(R.id.txtNumeroOpciones1)
        txtNumero2 = findViewById<EditText>(R.id.txtNumeroOpciones2)
        txtNumero3 = findViewById<EditText>(R.id.txtNumeroOpciones3)
        btnAceptar.setOnClickListener{
            val db = FechaDbHelper(this)
            strNumero = "${txtNumero1.text}-${txtNumero2.text}-${txtNumero3.text}"
            db.updateParametroNumero(strNumero)
            finish()
        }
        btnCancelar.setOnClickListener {
            finish()
        }
        txtNumero1.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if(s?.length == 4){
                    txtNumero2.requestFocus()
                }
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
        txtNumero2.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if(s?.length == 3){
                    txtNumero3.requestFocus()
                }
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
        inicializaNumeros()
  }
    private fun inicializaNumeros(){
        try {
            txtNumero1.requestFocus()
            val bundle = intent.extras
            strNumero = bundle.getString("Numero")
            txtNumero1.setText(strNumero.substring(0,4))
            txtNumero2.setText(strNumero.substring(5,8))
            txtNumero3.setText(strNumero.substring(9,13))
        }catch (ex: Exception){
        }
    }
}
