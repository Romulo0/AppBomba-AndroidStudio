package com.example.romul.appbomba

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

/**
 * Created by romul on 26/02/2018.
 */
class Recycler : RecyclerView.Adapter<viewHolder>() {
    var opciones : MutableList<menuConfig> = ArrayList()
    lateinit var context : Context
    fun Recycler(_opciones : MutableList<menuConfig>,_context : Context){
        this.opciones = _opciones
        this.context = _context
    }
    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        val item = opciones.get(position)
        holder.bind(item,context,position)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return viewHolder(layoutInflater.inflate(R.layout.recyclerlayout,parent,false))
    }
    override fun getItemCount(): Int{
        return opciones.size
    }

}
class viewHolder(view:View) : RecyclerView.ViewHolder(view){
    var dia:TextView = view.findViewById(R.id.Vdia)
    var horai:TextView = view.findViewById(R.id.Vinicio)
    var horaf:TextView = view.findViewById(R.id.Vfinal)
    //val personaImagen = view.findViewById<ImageView>(R.id.Vproceso)
    fun bind (menu:menuConfig, context:Context,position:Int){
        dia.text = menu.Dia
        horai.text = menu.horaInicio
        horaf.text = menu.horafinal
        itemView.setOnClickListener {
            val dia:String
            when(position){
                0 -> dia = "Lunes" 1 -> dia = "Martes" 2 -> dia = "Miercoles" 3 -> dia = "Jueves"
                4 -> dia = "Viernes" 5 -> dia = "Sabado" 6 -> dia = "Domingo"
                else -> dia = ""
            }
           val intent = Intent(context,MenuHoras::class.java)
            intent.putExtra("dia",dia)
            startActivity(context,intent,null)
        }
        // personaImagen.loadUrl(persona.imagen)
    }
    /*private fun ImageView.loadUrl(url: String){
        Picasso.with(context).load(url).into(this)
    }*/
}